<?php
// It is important for any file that includes this file, to have
// check_login_status.php included at its very top.

//check for new pm's
$pm_n = '<img src="images/pmStill.gif" width="17" height="12" alt="Pm" title="This pm is for logged in members">';
$sql = "SELECT id FROM private_messages WHERE (receiver='$log_username' AND parent='x' AND has_replies='1' AND sender_read='0') OR
        (sender='$log_username' AND sender_delete='0' AND parent='x' AND has_replies='1' AND sender_read='0') LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $numrows = mysqli_num_rows($query);
    if ($numrows > 0) {
        $pm_n = '<a href="pm_inbox.php?u= ' . $log_username . '" title="Private Message Notifier"><img src="images/pmFlash.gif" width="17" height="12" alt="Pm"></a>';
    } else {
        $pm_n = '<a href="pm_inbox.php?u= ' . $log_username . '" title="Private Message Notifier"><img src="images/pmStill.gif" width="17" height="12" alt="Pm"></a>';
    }
//Blank image for no friend request
$new_friends = '<a href="notifications.php" title="Friend Requests"><img src="images/nonewfriend.png" width="18" height="12" alt="Friend">(0)</a>&nbsp;&nbsp;&nbsp;';
$envelope = '<img src="images/note_dead.jpg" width="22" height="12" alt="Notes" title="This envelope is for logged in members">';
$loginLink = '<a href="login.php">Log In</a> &nbsp; | &nbsp; <a href="signup.php">Sign Up</a>';
if($user_ok == true) {
    $sql = "SELECT notescheck FROM users WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    $notescheck = $row[0];
    $sql = "SELECT id FROM notifications WHERE username='$log_username' AND date_time > '$notescheck' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $numrows = mysqli_num_rows($query);
    if ($numrows == 0) {
        $envelope = '<a href="notifications.php" title="Your notifications and friend requests"><img src="images/note_still.jpg" width="22" height="12" alt="Notes"></a>';
    } else {
        $envelope = '<a href="notifications.php" title="You have new notifications"><img src="images/note_flash.gif" width="22" height="12" alt="Notes"></a>';
    }
    $loginLink = '<a href="user.php?u='.$log_username.'">'.$log_username.'</a> &nbsp; | &nbsp; <a href="logout.php">Log Out</a>';
    //// ADDED THIS FOR FRIEND REQUEST COUNT AND DISPLAY
    $sql = "SELECT COUNT(id) FROM friends WHERE user2='$log_username' AND accepted='0'";
    $query = mysqli_query($db_conx, $sql); //or die("Error: ".mysqli_error($db_conx));
    $row = mysqli_fetch_row($query);
    $request_count = $row[0];
    if ($request_count != 0) {
        $new_friends = '<a href="notifications.php" title="Friend Requests"><img src="images/newfriend.png" width="18" height="12" alt="Friend">(' . $request_count . ')</a>&nbsp;&nbsp;&nbsp;';
    }
}
?>
<div id="pageTop">
    <div id="pageTopWrap">
        <div id="pageTopLogo">
            <a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/"><img src="images/logo.png" alt="logo" title="Web Intersect 2.0"/></a>
        </div>
        <div id="pageTopRest">
            <div id="menu1">
                <div>
                    <?php echo $pm_n; ?> <?php echo $new_friends; ?><?php echo $envelope; ?> &nbsp; &nbsp; <?php echo $loginLink; ?>
                </div>
            </div>
            <div id="menu2">
                <div>
                    <a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/"><img src="images/home.png" alt="home" title="Home" /></a>
                    <a href="#">Menu_Item_1</a>
                    <a href="#">Menu_Item_2</a>
                </div>
            </div>
        </div>
    </div>
</div>