<!DOCTYPE html>
<html>
    <head>
        <title>404 - Not found</title>
        <meta charset="utf-8">

        <link rel="stylesheet" href="style/style.css">
    </head>
    <body>
        <?php include_once('template_pageTop.php'); ?>
        <h1>Sorry, nothing to see here!</h1>
        <p>The Page you were looking for seems to be non-existing, we are sorry.</p>
        <a href="index.php">>> Back Home <<</a>
    </body>
</html>