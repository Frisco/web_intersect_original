<?php
    include_once('php_includes/check_login_status.php');
    $query = '';
    $sql = "SELECT username, avatar FROM users WHERE activated='1' ORDER BY RAND() LIMIT 32";
    $query = mysqli_query($db_conx, $sql); //or die("Error: ".mysqli_error($db_conx));
    $usernumrows = mysqli_num_rows($query);
    $userlist = '';
    while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
        $u = $row['username'];
        $avatar = $row['avatar'];
        $profile_pic = 'user/' . $u . '/' . $avatar;
        $userlist .= '<a href="user.php?u=' . $u . ' title=' . $u . '"><img src="' . $profile_pic . '" alt="' . $u . '" style="width=100px; height:100px; margin:10px;"></a>';
    }
    $usercount = '';
    $sql = "SELECT COUNT(id) FROM users WHERE activated='1'";
    $query = mysqli_query($db_conx, $sql); //or die("Error: ".mysqli_error($db_conx));
    $row = mysqli_fetch_row($query);
    $usercount = $row[0];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Web Intersect Social Network Tutorials and Demo</title>
        <meta charset="utf-8">

        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="style/style.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
    </head>

    <body>
        <?php include_once('template_pageTop.php'); ?>
        <div id="pageMiddle" class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                    <h3>Total Users: <?php echo $usercount; ?></h3>
                    <h3>Random People Chosen From Our Database That Have Uploaded Their Avatar</h3>
                    <?php echo $userlist; ?>
                    <hr />
                    <?php
                        if (!file_exists('test.txt')) {
                            echo '<span title="Sorry this File is currently not available">Text.txt</span>';
                        } else {
                            echo '<a href="text.txt" title="Text.txt">Text.txt</a>';
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('template_pageBottom.php'); ?>
    </body>
</html>