<?php
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASSWORD', '');
    define('DB_NAME', 'social');

    $db_conx= mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD , DB_NAME);

    // Evaluate the connection
    if (mysqli_connect_errno()) {
        echo mysqli_connect_error();
        exit();
    }
?>